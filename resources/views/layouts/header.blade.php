<div class="" style="background: content-box radial-gradient(yellow, skyblue);">
<div class="container-fluid ">
    <nav class="navbar navbar-light" >
        <a class="navbar-brand "  href="{{\Illuminate\Support\Facades\URL::to('/')}}">
            <img  src="{{asset('images/pngwing.com.png')}}" width="50" height="50" alt="" class="float-left mr-3">
            <h1>My Glasses</h1>
        </a>
        <ul class="nav justify-content-end p-3">
            <li class="nav-item">
                <a class="nav-link  active" href="#"><h5>Active</h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="#"><h5>Link</h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="#"><h5>Link</h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"><h5>Disabled</h5></a>
            </li>
        </ul>

        <form class="form-inline" action="{{route('glass-search')}}" method="get">
            <input value="{{old('common')}}" required name="common" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

    </nav>
</div>
</div>
