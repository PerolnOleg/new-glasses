<nav class="nav flex-column pl-3">
    <h4 class="text-center">Categories</h4>
    <div class="side-bar-wrap mb-3">
        @foreach($categories as $category)
            <a style="background: content-box radial-gradient(yellow, skyblue);" class="nav-link mt-3 mb-1"
               href="{{route('category',['category' => $category->name])}}"><h5
                    class="text-center mb-0">{{ucwords($category->name)}}</h5></a>
        @endforeach
    </div>

    <h4 class="text-center">Brands</h4>
    <div class="side-bar-wrap mb-3">
        @foreach($brands as $brand)
            <a style="background: content-box radial-gradient(yellow, skyblue);" class="nav-link mt-3 mb-1"
               href="{{route('brand',['brand' => $brand->slug])}}"><h5
                    class="text-center mb-0">{{ucwords($brand->name)}}</h5></a>
        @endforeach
    </div>

</nav>
