<!doctype html>
<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
@include('layouts.header')

<div class="d-flex align-self-stretch"  style="background: content-box radial-gradient(yellow, skyblue);">
    <div class="bd-highlight col-2">
        @include('layouts.sidebar')
    </div>
    <div class="col-10 p-5">
        @include('layouts.errors')
        @yield('content')
    </div>
</div>
@include('layouts.footer')
</body>
</html>
