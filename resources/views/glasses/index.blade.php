@extends('layouts.app')
@section('content')
    <h1>The main content</h1>
    <div class="d-flex flex-wrap">
        @foreach($glasses as $glass)
            @if($glass->image()->exists())
                @include('glasses.card')
            @endif
        @endforeach
    </div>
    <div class="d-flex m-5">
        <div class="mx-auto">
            {{$glasses->appends(request()->input())->links()}}
        </div>
    </div>
@endsection
