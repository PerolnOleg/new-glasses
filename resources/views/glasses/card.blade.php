<div class="card m-2" style="width: 20rem;">
    <img class="card-img-top" src="{{$glass->image->src}}" alt="Card image cap">
    <div class="card-body d-flex align-items-start flex-column bd-highlight">
        <div class=" bd-highlight"><h5 class="card-title"><strong>{{ucwords($glass->name)}}</strong></h5></div>
        <div class=" bd-highlight"><h6 class="card-title">category: {{$glass->category_id}} &nbsp brand: {{$glass->brand_id}}</h6></div>
        <div class=" bd-highlight"><h6 class="card-title"></h6></div>
        <div class="mb-auto bd-highlight"><p class="card-text"><em>{{\Illuminate\Support\Str::limit($glass->description, 50)}}</em></p></div>
        <div class="d-flex w-100 mt-3">
            <a href="#" class="btn btn-primary pl-4 pr-4">Buy</a>
            <h3 class="ml-auto mb-0 pt-1">${{$glass->price}}</h3>
        </div>
    </div>
</div>
