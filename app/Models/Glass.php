<?php

namespace App\Models;

use App\Filters\GlassFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\{Builder, Model, SoftDeletes};
use Illuminate\Http\Request;


class Glass extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {

        return $this->hasMany(Image::class);

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(Image::class)
            ->where('order', '=', 0)
            ->where('active', '=', 1);
    }

    /**
     * @param int|null $value
     * @return float
     */
    public function getPriceAttribute(?int $value = 0): float
    {
        return number_format($value / 100, 2);
    }

    /**
     * @param Builder $builder
     * @param Request $request
     * @return Builder
     */
    public function scopeFilter(Builder $builder, Request $request)
    {
        return ($this->resolveFilter($request))->filter($builder);
    }

    /**
     * @param Builder $builder
     * @param Request $request
     * @return Builder
     */
    public function scopeRelationFilter(Builder $builder, Request $request)
    {
        return ($this->resolveFilter($request))->relationFilter($builder);
    }

    /**
     * @param Builder $builder
     * @param Request $request
     * @return Builder
     */
    public function scopeCommonFilter(Builder $builder, Request $request)
    {
        return ($this->resolveFilter($request))->multiFilter($builder);
    }

    /**
     * @param Request $request
     * @return GlassFilter
     */
    private function resolveFilter(Request $request){
        return new GlassFilter($request);
    }
}
