<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\{Builder, Model, SoftDeletes};

class Category extends Model
{
    use HasFactory, SoftDeletes;

    public function glasses(): Builder{
        return $this->hasMany(Glass::class);
    }
}
