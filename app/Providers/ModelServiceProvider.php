<?php

namespace App\Providers;


use App\Http\Controllers\{GlassesController, GlassSearchController};
use App\Interfaces\{BrandRepositoryInterface, CategoryRepositoryInterface, GlassRepositoryInterface, ModelsRepository};
use App\Repositories\{BrandEloquentRepository, CategoryEloquentRepository, GlassEloquentRepository};
use Illuminate\Support\ServiceProvider;

class ModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GlassRepositoryInterface::class, GlassEloquentRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryEloquentRepository::class);
        $this->app->bind(BrandRepositoryInterface::class, BrandEloquentRepository::class);
        $this->app->when([GlassesController::class, GlassSearchController::class])->needs(ModelsRepository::class)->give(GlassEloquentRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
