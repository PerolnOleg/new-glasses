<?php


namespace App\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class AbstractFilter
{
    protected Request $request;
    protected array $filters = [];
    protected array $relation_filters = [];


    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function filter(Builder $builder)
    {
        foreach ($this->getFilters() as $filter => $value) {
            $this->resolveFilter($filter)->filter($builder, $value);
        }
        return $builder;
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function relationFilter(Builder $builder)
    {
        foreach ($this->getRelationFilters() as $filter => $value) {

            $this->resolveRelationFilter($filter)->filter($builder, $value);
        }
        return $builder;
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function multiFilter(Builder $builder)
    {
        $common_value = $this->request->input('common');
        $common_value ??= ' ';
        foreach (array_keys($this->filters) as $filter) {
            $this->resolveFilter($filter, 1)->filter($builder, $common_value);
        }
        return $builder;
    }

    /**
     * @return array
     */
    protected function getFilters()
    {
        return array_filter($this->request->only(array_keys($this->filters)));
    }

    /**
     * @return array
     */
    protected function getRelationFilters()
    {
        $collect = collect($this->request->validated());
        return array_filter($collect->only(array_keys($this->relation_filters))->toArray());
    }

    /**
     * @param $filter
     * @param bool $common
     * @return mixed
     */
    protected function resolveFilter($filter, $common = false)
    {
        return new $this->filters[$filter]($common);
    }

    /**
     * @param $filter
     * @param bool $common
     * @return mixed
     */
    protected function resolveRelationFilter($filter, $common = false)
    {
        return new $this->relation_filters[$filter]($common);
    }

}
