<?php


namespace App\Filters;


use App\Filters\Glass\{DescriptionFilter, GlassBrandFilter, GlassCategoryFilter, NameFilter, ModelFilter};

class GlassFilter extends AbstractFilter
{
    protected array $filters = [
        'name' => NameFilter::class,
        'model' => ModelFilter::class,
        'description' => DescriptionFilter::class
    ];

    protected array $relation_filters = [
        'category' => GlassCategoryFilter::class,
        'brand' => GlassBrandFilter::class
    ];

}
