<?php


namespace App\Filters;


use Illuminate\Database\Eloquent\Builder;

abstract class AbstractQueryFilter
{
    protected bool $common_query;
    public function __construct(bool $common_query = false)
    {
        $this->common_query = $common_query;
    }

    abstract public function filter(Builder $builder, string $value): Builder;
}
