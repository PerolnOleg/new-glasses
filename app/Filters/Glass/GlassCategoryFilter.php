<?php


namespace App\Filters\Glass;


use App\Filters\AbstractQueryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class GlassCategoryFilter extends AbstractQueryFilter
{
    /**
     * @param Builder $builder
     * @param string $value
     * @return Builder
     */
    public function filter(Builder $builder, string $value): Builder
    {
            return $builder->join('categories', function($join) use($value){
                $join->on('glasses.category_id', '=', 'categories.id')
                    ->where('categories.name' , 'LIKE', $value);
            })->select('glasses.*');
    }

}
