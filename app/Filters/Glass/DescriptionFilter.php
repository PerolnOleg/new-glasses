<?php


namespace App\Filters\Glass;


use App\Filters\AbstractQueryFilter;
use Illuminate\Database\Eloquent\Builder;

class DescriptionFilter extends AbstractQueryFilter
{
    /**
     * @param Builder $builder
     * @param $value
     * @return Builder
     */
    public function filter(Builder $builder, string $value): Builder
    {
        if($this->common_query){
            return $builder->orWhere('description', 'LIKE', '%' . $value . '%');
        }
        return $builder->where('description', 'LIKE', '%' . $value . '%');
    }
}
