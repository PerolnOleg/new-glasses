<?php


namespace App\Filters\Glass;


use App\Filters\AbstractQueryFilter;
use Illuminate\Database\Eloquent\Builder;

class NameFilter extends AbstractQueryFilter
{
    /**
     * @param $builder
     * @param $value
     * @return mixed
     */
    public function filter(Builder $builder, string $value): Builder
    {
        if($this->common_query){
            return $builder->orWhere('name', 'LIKE', '%'.$value.'%');
        }
        return $builder->where('name', 'LIKE', '%'.$value.'%');
    }

}
