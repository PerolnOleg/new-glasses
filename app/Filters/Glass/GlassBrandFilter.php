<?php


namespace App\Filters\Glass;


use App\Filters\AbstractQueryFilter;
use Illuminate\Database\Eloquent\Builder;

class GlassBrandFilter extends AbstractQueryFilter
{

    public function filter(Builder $builder, string $value): Builder
    {
        return $builder->join('brands', function($join) use($value){
            $join->on('glasses.brand_id', '=', 'brands.id')
                ->where('brands.slug' , 'LIKE', $value);
        })->select('glasses.*');
    }
}
