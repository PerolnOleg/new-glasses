<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategorySearchRequest;
use App\Http\Requests\GlassBrandRequest;
use App\Http\Requests\GlassCategoryRequest;
use App\Http\Requests\GlassSearchRequest;
use Illuminate\Http\Request;

class GlassSearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(GlassSearchRequest $request)
    {
        $glasses = $this->repository->commonFilter($request)->with('image')->paginate(config('view.paginate.glass-search'));
        $request->flash();
        return view('glasses.index', compact('glasses'));
    }

    /**
     * @param GlassCategoryRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function category(GlassCategoryRequest $request)
    {

        $glasses = $this->repository->relationFilter($request)->with('image')->paginate(config('view.paginate.glass-search'));
        $request->flash();
        return view('glasses.index', compact('glasses'));
    }

    /**
     * @param GlassBrandRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function brand(GlassBrandRequest $request)
    {
        $glasses = $this->repository->relationFilter($request)->with('image')->paginate(config('view.paginate.glass-search'));
        $request->flash();
        return view('glasses.index', compact('glasses'));
    }
}
