<?php

namespace App\Http\Controllers;

use App\Interfaces\ModelsRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $repository;

    /**
     * Controller constructor.
     * @param ModelsRepository $repository
     */
    public function __construct(ModelsRepository $repository)
    {
        $this->repository = $repository;
    }
}
