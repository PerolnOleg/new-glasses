<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class GlassSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'common' => 'string|max:100|required_without_all:description,name,model',
            'description' => 'string|required_without_all:common,name,model',
            'name' => 'string|required_without_all:common,model,description',
            'model' => 'numeric|required_without_all:common,name,description'
        ];
    }
}
