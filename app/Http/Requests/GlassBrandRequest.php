<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GlassBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'required|exists:brands,slug'
        ];
    }

    /**
     * @return GlassBrandRequest|string
     */
    protected function prepareForValidation()
    {
        $this->merge([
           'brand' => $this->route('brand')
        ]);
    }
}
