<?php


namespace App\Repositories;


use App\Interfaces\CategoryRepositoryInterface;
use App\Models\Category;

class CategoryEloquentRepository implements CategoryRepositoryInterface
{

    public function getAllModels()
    {
        return Category::where('active', 1)->get();
    }

    public function getModelById(int $id)
    {
        // TODO: Implement getModelById() method.
    }

    public function createModel()
    {
        // TODO: Implement createModel() method.
    }

    public function updateModel(int $id, array $params)
    {
        // TODO: Implement updateModel() method.
    }

    public function deleteModel(int $id)
    {
        // TODO: Implement deleteModel() method.
    }

    public function paginateModels(?array $requestArray = null)
    {
        // TODO: Implement paginateModels() method.
    }
}
