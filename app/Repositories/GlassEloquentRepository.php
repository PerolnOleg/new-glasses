<?php


namespace App\Repositories;


use App\Http\Requests\GlassSearchRequest;
use App\Interfaces\ModelsRepository;
use App\Models\Glass;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class GlassEloquentRepository implements ModelsRepository
{
    /**
     * @return Glass[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllModels()
    {
        return Glass::all();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getModelById(int $id)
    {
        return Glass::firstOrFail($id);
    }

    public function createModel()
    {

    }

    public function updateModel(int $id, array $params)
    {
        $model = $this->getModelById($id);
        $model->fill($params);
        $model->save();
    }

    /**
     * @param int $id
     * @return bool|null|\Exception
     */
    public function deleteModel(int $id): ?bool
    {
        $model = $this->getModelById($id);
        return $model->delete();
    }

    /**
     * @param array|null $requestArray
     * @return mixed
     */
    public function paginateModels(?array $requestArray = null)
    {
        return Glass::with('image')->whereHas('image')->paginate(config('view.paginate.glass'));
    }

    /**
     * @param GlassSearchRequest $request
     * @return Builder
     */
    public function commonFilter(GlassSearchRequest $request): Builder
    {
        if ($request->has('common')) {
            return Glass::CommonFilter($request);
        }
        return Glass::filter($request);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function relationFilter(Request $request)
    {
        return Glass::RelationFilter($request);
    }
}
