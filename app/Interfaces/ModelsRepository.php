<?php


namespace App\Interfaces;


interface ModelsRepository
{
    public function getAllModels();

    public function getModelById(int $id);

    public function createModel();

    public function updateModel(int $id, array $params);

    public function deleteModel(int $id);

    public function paginateModels(?array $requestArray = null);
}
