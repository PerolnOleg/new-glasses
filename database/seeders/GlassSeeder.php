<?php

namespace Database\Seeders;

use App\Models\Glass;
use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GlassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = DB::table('brands')->where('active', '1')->pluck('id')->toArray();
        $categories = DB::table('categories')->where('active', '1')->pluck('id')->toArray();
        try {
            Glass::factory()
                ->count('1000')
                ->state(new Sequence(function () use ($brands, $categories) {
                    return [
                        'brand_id' => Arr::random($brands),
                        'category_id' => Arr::random($categories)
                    ];
                }))
                ->has( Image::factory()->count(3)->state(new Sequence(
                    ['order' => '0'],
                    ['order' => '1'],
                    ['order' => '2'],
                )))
            ->create();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $this->command->error('The Glass seeder was fail!');
        }

    }
}
