<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('categories')->insert([
          ['name' => 'male',
              'created_at' => now()],
          ['name' => 'female',
              'created_at' => now()],
          ['name' => 'unisex',
              'created_at' => now()],
          ['name' => 'child',
              'created_at' => now()],
       ]);
    }
}
