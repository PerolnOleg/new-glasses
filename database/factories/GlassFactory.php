<?php

namespace Database\Factories;

use App\Models\Glass;
use Illuminate\Database\Eloquent\Factories\Factory;

class GlassFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Glass::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->words(2, true),
            'model' => $this->faker->regexify('[A-Z]{5}[0-4]{3}'),
            'price' => $this->faker->numberBetween(100, 100000),
            'description' => $this->faker->realText(200, 2),
        ];
    }
}
