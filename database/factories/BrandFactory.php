<?php

namespace Database\Factories;

use App\Models\Brand;
use Illuminate\Database\Eloquent\Factories\Factory;

class BrandFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Brand::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->unique()->company();
        $slug = strtolower($name);
        $slug =  preg_replace('#[[:punct:]]#', ' ', $slug);
        $slug = preg_replace('/[\s]+/', '-', $slug);

        return [
            'name' => $name,
            'description' => $this->faker->realText($maxNbChars = 255, $indexSize = 2),
            'address' => $this->faker->address(),
            'slug' => $slug
        ];
    }
}
