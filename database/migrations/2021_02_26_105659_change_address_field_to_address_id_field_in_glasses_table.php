<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAddressFieldToAddressIdFieldInGlassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('glasses', function (Blueprint $table) {
            $table->renameColumn('brand', 'brand_id');
        });
        Schema::table('glasses', function (Blueprint $table) {
            $table->foreignId('brand_id')->change()->constrained();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('glasses', function (Blueprint $table) {
            $table->dropForeign(['brand_id']);
        });
        Schema::table('glasses', function (Blueprint $table) {
            $table->string('brand_id', 100)->change();
        });
        Schema::table('glasses', function (Blueprint $table) {
            $table->renameColumn('brand_id', 'brand');
        });

    }
}
