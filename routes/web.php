<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{GlassesController, GlassSearchController, CategorySearchController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GlassesController::class, 'index'])->name('index');
Route::get('/glass-search', [GlassSearchController::class, 'index'])->name('glass-search');
Route::get('/category/{category}', [GlassSearchController::class, 'category'])->name('category');
Route::get('/brand/{brand}', [GlassSearchController::class, 'brand'])->name('brand');
